#!/bin/bash
BUILDNUMBER=$1
if [ -d appserver/addons ]; then
    echo "Deleteing the Addons Folder"
    rm -rf ./appserver/addons
fi
if [ ! -d appserver/addons ]; then
    echo "Making Addons Folder"
    mkdir appserver/addons
fi

for viz in appserver/static/visualizations/*;
do
    VIZNAME=$(echo "$viz" | awk -F'/' '{print $4}')
    echo "$VIZNAME"
    APP_CONF="default/app.conf"

    CMD="sed -i'' 's/is_visible = true/is_visible = false/' $APP_CONF"
    echo "Running $CMD"
    eval $CMD
    CMD="sed -i'' 's/description.*/description = A Custom Visualization, $VIZNAME/' $APP_CONF"
    echo "Running $CMD"
    eval $CMD
    CMD="sed -i'' 's/label.*/label = Custom Visualization - $VIZNAME/' $APP_CONF"
    echo "Running $CMD"
    eval $CMD
    CMD="sed -i'' 's/build.*/build = $BUILDNUMBER/' $APP_CONF"
    echo "Running $CMD"
    eval $CMD
    CMD="sed -i'' 's/id.*/id = aplura_viz_$VIZNAME/' $APP_CONF"
    echo "Running $CMD"
    eval $CMD
    echo "Removing Dot Files"
    find . -name ".*" -print0 -exec rm {} \;
    echo "\n"
    echo "Removing Python Files"
    find . -name "*.py*" -print0 -exec rm {} \;
    echo "\n"
    echo "Removing Zero Length Files"
    find . -size 0 -print0 -exec rm {} \;
    echo "\n"
    FILEOUT="aplura_viz_${VIZNAME}_b${BUILDNUMBER}.spl"
    FOLDERIN="aplura_viz_$VIZNAME"
done
