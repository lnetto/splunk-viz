/*
 * Visualization source
 */
define([
            'jquery',
            'underscore',
            'api/SplunkVisualizationBase',
            'api/SplunkVisualizationUtils',
            'd3',
            'util/general_utils'
        // Add required assets to this list
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            vizUtils,
            d3,
            genUtils
        ) {
  
    // Extend from SplunkVisualizationBase
    return SplunkVisualizationBase.extend({
  
        initialize: function() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
            this.$el = $(this.el);

            this.$el.append('<h3>This is a custom visualization stand in.</h3>');
            this.$el.addClass('splunk-funnel');
        },

        // Optionally implement to format data returned from search. 
        // The returned object will be passed to updateView as 'data'
        formatData: function(data) {

            if (!data || data.rows.length < 1) {
                return false;
            }

            var result = [];
            var rows = data.rows;

            if (data.fields.length < 2) {
                throw new SplunkVisualizationBase.VisualizationError(
                    'Check the Statistics tab. To generate a Funel graph, the table must include columns representing values in these two fields: <name>, <value>'
                );
            }

            this.splitFieldName = data.fields[0]['name'];

            _.each(rows, function(row) {
                if (_.isNaN(+row[1])) {
                    throw new SplunkVisualizationBase.VisualizationError(
                        'Check the Statistics tab. To generate a Funnel graph, values in the <value> field must be numeric.'
                    );
                }
                var cur = [vizUtils.escapeHtml(row[0]), +row[1],+row[2]];
                result.push(cur);
            });

            //console.log("isArray ::: " + Array.isArray(result));

            return result;
        },
  
        // Implement updateView to render a visualization.
        //  'data' will be the data object returned from formatData or from the search
        //  'config' will be the configuration property object
        updateView: function(data, config) {

            if (!data || data.length < 1) {
                return
            }

            var D3Funnel = require('d3-funnel');
            this.$el.empty();

            var curved = genUtils.normalizeBoolean(this._getEscapedProperty('curvedFunnel', config)) || false;
            var inverted = genUtils.normalizeBoolean(this._getEscapedProperty('invertedFunnel', config)) || false;
            var baroverlay = genUtils.normalizeBoolean(this._getEscapedProperty('barOverlay', config)) || false;
            var filltype = this._getEscapedProperty('fillType', config) || 'gradient';
            var highlight = genUtils.normalizeBoolean(this._getEscapedProperty('highlight', config)) || false;
            var fontSize = this._getEscapedProperty('fontsize', config) || '14';
            var labelcolor = this._getEscapedProperty('labelcolor', config) || '#ffffff';
            var unit = this._getEscapedProperty('unit', config) || '';
            var highlight = genUtils.normalizeBoolean(this._getEscapedProperty('highlight', config)) || false;

            var color = d3.scaleOrdinal().domain([0, 6000]).range(['#ffffff', '#34be88']);

            var title = '{l}: ' ;

            var options = { 
                block: { 
                    dynamicHeight: true,
                    minHeight:20,
                    barOverlay:baroverlay,
                    highlight: highlight,
                    fill:{
                        type:filltype,
                    },
                }, 
                label: {
                    format: title + '{f} ' + unit,
                    fontSize : fontSize,
                    fill : labelcolor,

                }, 
                chart: {    
                    inverted: inverted,
                    animate:3,
                    height:'90%',
                    width:'90%',
                    curve: {
                        enabled:curved,
                    },
                },
                events: {
                    click: {
                        block: (data) => {
                           // console.log(data);
                        }
                    },
                }
            };
           

            var chart = new D3Funnel(this.el);

            chart.draw(data, options);

        },


        // Search data params
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.ROW_MAJOR_OUTPUT_MODE,
                count: 10000
            });
        },

        // Override to respond to re-sizing events
        reflow: function() {
            this.invalidateUpdateView();
        },


        // Escape HTML entities
        _getEscapedProperty: function(name, config) {
            var propertyValue = config[this.getPropertyNamespaceInfo().propertyNamespace + name];
            return (vizUtils.escapeHtml(propertyValue));
        },

        
        myaction: function(data) {
            console.log("Je passe ici !!!! " + data);
        }


    });
});
