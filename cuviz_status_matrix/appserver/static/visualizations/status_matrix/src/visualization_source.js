/*
 * Visualization source
 */
define([
            'jquery',
            'underscore',
            'vizapi/SplunkVisualizationBase',
            'vizapi/SplunkVisualizationUtils',
            'd3',
            'd3-tip',
            'd3-bubble-matrix'
            // Add required assets to this list
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            SplunkVisualizationUtils,
            d3,
            d3tip,
            BubbleMatrix,
            Drilldown
        ) {
  
    // Extend from SplunkVisualizationBase
    return SplunkVisualizationBase.extend({
  
        initialize: function() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);

            d3.tip = d3tip;

            this.$el = $(this.el);

            this.$el.addClass('splunk-status-matrix');

            this.matrix = null;
        },

        // Optionally implement to format data returned from search. 
        // The returned object will be passed to updateView as 'data'
        formatData: function(data) {
            if (data.columns.length < 1) {
                return false;
            }

            status_columns = _.rest(data.columns);
            _.each(status_columns, function(status_column) {
                _.each(status_column, function(value, index) {
                    if (value == null) {
                        status_column[index] = -1;
                    }
                });
            });

            // console.log(data);

            return data;
        },
  
        // Implement updateView to render a visualization.
        //  'data' will be the data object returned from formatData or from the search
        //  'config' will be the configuration property object
        updateView: function(data, config) {
            if (!data) {
                return;
            }

            this.$el.empty();

            if (this.matrix) {
                this.matrix.tooltip.hide();
            }

            if (data.columns.length < 2) {
                throw new SplunkVisualizationBase.VisualizationError(
                    'Need at least two columns status data'
                );
            }

            var radius = +config['display.visualizations.custom.cuviz_status_matrix.status_matrix.radius'] || 24;

            if (radius <= 0 || radius > 30) {
                throw new SplunkVisualizationBase.VisualizationError(
                    'The radius should be in [1, 30]'
                );
            }

            var that = this;

            this.matrix = new BubbleMatrix({
                selector: "div[data-cid='" + this.$el.data("cid") + "'].splunk-status-matrix",
                width: this.$el.width(),
                height: Math.max(this.$el.height() - 20, 180),
                hideRightTitle: true,
                hideTopTitle: true,
                maxColors: 3,
                maxRadius: radius,
                duration: 1000,
                onClick: function (val) {
                    $('.splunk-status-matrix g.bubbles circle').removeClass('selected');
                    $(d3.event.target).addClass('selected');

                    var payload = {
                        action: SplunkVisualizationBase.FIELD_VALUE_DRILLDOWN,
                        data: {}
                    };

                    cx = $(d3.event.target).attr('cx')
                    cy = $(d3.event.target).attr('cy')
                    payload.data['x'] = $('.splunk-status-matrix g.bottom-columns text.column[x="' + cx + '"]').text().trim()
                    payload.data['y'] = $('.splunk-status-matrix g.left-rows text.row[y="' + cy + '"]').text().trim()
                    payload.data['status'] = val
                    
                    that.drilldown(payload, d3.event);
                },
                tooltip: d3.tip().attr('class', 'd3-tip').offset([-10, 0]).html(
                    function (value) {
                      status = value == 1 ? 'Passed' : value == -1 ? 'Unknown' : 'Failed';
                      return 'Status: ' + status;
                    })
            });

            this.matrix.init = function() {
                this.columns = this.data.columns;
                this.rows = this.data.rows;
                this.scale = {};

                this.scale.y = d3.scale.ordinal().domain(d3.range(0, this.rows.length)).rangePoints([0, this.height], this.VERTICAL_PADDING);

                this.scale.color = d3.scale.quantize().domain(this.COLOR_DOMAIN).range(d3.range(1, this.MAX_COLORS + 1));

                this.scale.radius = d3.scale.sqrt().range([this.MAX_RADIUS, this.MAX_RADIUS]);
                return this;
            }

            this.matrix.COLOR_DOMAIN = [-1, 1];

            var matrix_data = {
                columns: _.first(data.columns),
                rows: _.map(_.rest(data.columns), function(column_values, idx) {
                    return { name: data.fields[idx+1]['name'], values: column_values};
                })
            };

            this.matrix.draw(matrix_data);

        },

        reflow: function() {
            this.invalidateUpdateView();
        },

        // Search data params
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.COLUMN_MAJOR_OUTPUT_MODE,
                count: 10000
            });
        }
    });
});