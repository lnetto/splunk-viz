/*
 * Visualization source
 */
define([
            'jquery',
            'underscore',
            'vizapi/SplunkVisualizationBase',
            'vizapi/SplunkVisualizationUtils',
            'd3'
            // Add required assets to this list
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            vizUtils,
            d3
        ) {
  
    // Extend from SplunkVisualizationBase
    return SplunkVisualizationBase.extend({
  
        initialize: function() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
            this.$el = $(this.el);

            this.$el.append('<h3>This is a custom visualization stand in.</h3>');
            this.$el.append('<p>Edit your custom visualization app to render something here.</p>');
            
            // Initialization logic goes here
        },

        // Optionally implement to format data returned from search. 
        // The returned object will be passed to updateView as 'data'
        formatData: function(data) {
            console.log(data);
            // Format data 
            // var newData = _.map(data.rows, function(d) { return [ d[0], [ Number(d[4]), Number(d[1]), Number(d[5]) ], [ Number(d[7]), Number(d[8]) ], [], Number(d[2]), Number(d[3]) ]; });
            var newData = _.map(data.results, function(d) { return [ d[data.fields[0].name], [ Number(d.lowerquartile), Number(d.median), Number(d.upperquartile) ], [ Number(d.lowerwhisker), Number(d.upperwhisker) ], [], Number(d.min), Number(d.max) ]; });
            console.log("newData = ", newData);

            return newData;
        },
  
        // Implement updateView to render a visualization.
        //  'data' will be the data object returned from formatData or from the search
        //  'config' will be the configuration property object
        updateView: function(data, config) {
            
            // Draw something here
if (!data) {
    return;
}
this.$el.empty();

var labels = true; // show the text labels beside individual boxplots?

var labeloffset = 75;
var margin = {top: 30, right: 50, bottom: 70 + labeloffset, left: 50};
var  width = 800 - margin.left - margin.right;
var height = 400 + labeloffset - margin.top - margin.bottom;
    
var min = Infinity,
    max = -Infinity;

// var newFakeData = [
  // Column  Quartiles        Whiskers  Outliers        Min  Max
//   ["somedata", [ 10, 20, 30 ],   [5, 45],  [1, 2],         0, 200],
//   ["otherdata", [ 15, 25, 30 ],   [5, 65],  [],             2, 150],
//   ["a1", [ 15, 25, 30 ],   [6, 65],  [3, 75],        1, 250],
//   ["b4", [ 15, 25, 30 ],   [12, 65], [],             8, 100],
//   ["thing", [ 25, 38, 42 ],   [17, 65], [10],           6, 300],
//   ["other", [ 30, 42, 120 ],   [23, 159], [220],          12, 300],
//   ["somestuff", [ 15, 25, 30 ],   [10, 65], [5],            0, 100],
// ];

console.log(JSON.stringify(data));


var yMinValues = [];
for (minind = 0; minind < data.length; minind++) {
        yMinValues.push(data[minind][2][0]);
} 
var yMin = Math.min(...yMinValues);

var yMaxValues = [];
for (maxind = 0; maxind < data.length; maxind++) {
        yMaxValues.push(data[maxind][2][1]);
} 
var yMax = Math.max(...yMaxValues);

console.log("yMin = ", yMin);
console.log("yMax = ", yMax);


var box = require("./box");

var chart = d3.box({"yMax":yMax,"yMin":yMin})
  .height(height)   
  .showLabels(labels);

var svg = d3.select(this.el).append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .attr("class", "box")    
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// the x-axis
var x = d3.scale.ordinal()     
  .domain(data.map(function(d){return d[0]}))
  .rangeRoundBands([0 , width], 0.7, 0.3);      

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom");

// the y-axis
var y = d3.scale.linear()
  // .domain([yMin, yMax])
  .domain([yMin, yMax])
  // .range([height + margin.top, 0 + margin.top]);
  .range([height + margin.top, yMin + margin.top]);

var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")

// draw the boxplots    
svg.selectAll(".box")      
    .data(data)
  .enter().append("g")
  .attr("transform", function(d) { return "translate(" +  x(d[0])  + "," + margin.top + ")"; } )
    .call(chart.width(x.rangeBand())); 

      
// add a title
svg.append("text")
      .attr("x", (width / 2))             
      .attr("y", 0 + (margin.top / 2))
      .attr("text-anchor", "middle")  
      .style("font-size", "18px") 
      //.style("text-decoration", "underline")  
      // .text("Revenue 2012");

 // draw y axis
svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
  .append("text") // and text1
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    // .style("font-size", "16px") 
        // .text("Y-axis");      

// draw x axis  
svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + (height  + margin.top + 10) + ")")
    .call(xAxis)
    .selectAll("text")
    .attr("transform","rotate(-45)")
        .style("text-anchor", "end")
  .append("text")             // text label for the x axis
      .attr("x", (width / 2) )
      .attr("y",  20 )
  .attr("dy", ".71em")
      .style("text-anchor", "end");
  // .style("font-size", "16px") 
      // .text("New"); 


        },

        // Search data params
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.RAW_OUTPUT_MODE,
                count: 10000
            });
        },

        // Override to respond to re-sizing events
        reflow: function() {}
    });
});
