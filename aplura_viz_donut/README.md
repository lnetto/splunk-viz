### Splunk Visualization - Donut Chart

> The donut chart is a variation of the standard pie chart. This chart will display results as a
> percentage of the whole.

#### How to Use the Donut Chart

> This chart has the ability to be displayed as either a two-dimensional or three-dimensional
> chart. This chart provides some interactivity. Mouseover a section of the chart to display
> the associated results. Click on a slice to drill down into the raw results.

#### Configuration Options

> The donut chart has many different configuration options. The options are split into sections
> related to each type of chart. The options are displayed like this:

* Configuration Option Name
    * Valid Setting Options
    * Description

##### Format

* Chart Type
    * 2D, 3D
    * Toggles between 2-dimensional and 3-dimensional
    
* Width
    * &lt;integer&gt;
    * Width of the SVG. If you have clipping due to the size then increase this.
    
* Height
    * &lt;integer&gt;
    * Height of the SVG. If you have clipping due to the size then increase this.
    
##### Legend

* Show Legend
    * Yes, No
    * Toggles the legend.
    
* Height (px)
    * &lt;integer&gt;
    * Height of the legend text.
    
* Position
    * Left, Right
    * Controls where the legend is placed, either to the left or right of the chart.
    
* Top Offset
    * &lt;integer&gt;
    * Controls how far from the top of the visualization the legend will display.
    
* Left Offset
    * &lt;integer&gt;
    * Controls how far from the left of the visualization the legend will display.
    
##### 2D Chart

* Radius
    * &lt;integer&gt;
    * Controls the radius of the chart.
    
* Donut Width
    * &lt;integer&gt;
    * Controls the width of the donut hole.
    
##### 3D Chart

* X Center
    * &lt;integer&gt;
    * Controls the center of the chart on the X axis.
    
* Y Center
    * &lt;integer&gt;
    * Controls the center of the chart on the Y axis.
    
* X Radius
    * &lt;integer&gt;
    * Controls the radius of the chart on the X axis.
    
* Y Radius
    * &lt;integer&gt;
    * Controls the radius of the chart on the Y axis.

* Height
    * &lt;integer&gt;
    * Controls the thickness of the chart.
    
* Inner Radius
    * &lt;float&gt;
    * Controls the width of the donut hole.
    
##### Drilldown

* Drilldown
    * Yes, No
    * Allow drilldown into results.
    
##### Debug

* Javascript Logging
    * Yes, No
    * Disables or enables the logging of debug information for the visualization.

#### Example search

Example: index=_internal | stats count by sourcetype

#### Support

Please ask a question on Answers. Tag it with "aplura_viz" to get noticed. Support URL: answers.splunk.com