/*
 * Visualization source
 */
define([
            'jquery',
            'underscore',
            'vizapi/SplunkVisualizationBase',
            'vizapi/SplunkVisualizationUtils',
            'd3'
            // Add required assets to this list
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            vizUtils,
            d3
        ) {

    // Extend from SplunkVisualizationBase
    return SplunkVisualizationBase.extend({

        initialize: function() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
            this.$el = $(this.el);
            this.$el.addClass("donut");
        },

        // Optionally implement to format data returned from search.
        // The returned object will be passed to updateView as 'data'
        formatData: function(data) {

            // Format data
            var total_events = 0;
            var percent = 0;

            for (var i = 0; i < data.rows.length; i++) {
                total_events += data.rows[i][1] << 0;
            }

            for (var idx = 0; idx < data.rows.length; idx++) {
                percent = 0;
                percent = Math.round(1000 * data.rows[idx][1] / total_events /10);
                if (percent <= 1) {
                    data.rows[idx][0] = "OTHER";
                }
            }

            return data;
        },
        _check_boolean: function (config_option) {
            var s = this._getEscapedProperty(config_option);
            console.log({"s": s, "config_option": config_option});
            return !(s == "0" || s == "false" || Number(s) === 0);
        },

        // Implement updateView to render a visualization.
        //  'data' will be the data object returned from formatData or from the search
        //  'config' will be the configuration property object
        updateView: function(data, config) {

            // Draw something here

            this._config = config;
            var do_logging = this._check_boolean("log");
            var allow_drilldown = this._getEscapedProperty("drilldown") || 'No';

            function do_log(s) {
                if (do_logging) {
                    console.log(s);
                }
            }

            do_log("updateView");
            do_log(allow_drilldown);
            if (!data || data.rows.length < 1) {
                return;
            }

            this.$el.empty();
            if (allow_drilldown == 'Yes') {
                this.$el.on('click');
            }
            else {
                this.$el.off('click');
            }

            //
            // Create array where all "OTHER" items are combined into one array item plus
            // all individual items
            //
            var pie_data = [];
            var other_count = 0;
            var max_x = 0, max_y = 0, temp_x = 0;

            for (var i = 0; i < data.rows.length; i++) {
                temp_x = data.rows[i][0].length;
                if (temp_x > max_x) {
                    max_x = temp_x;
                }
                if (data.rows[i][0] == "OTHER") {
                    other_count += parseInt(data.rows[i][1]);
                }
                else {
                    pie_data.push([data.rows[i][0], data.rows[i][1]]);
                }
            }

            if (other_count > 0) {
                pie_data.push(["OTHER", other_count]);
            }

            var chartType = this._getEscapedProperty("chartType") || "2D";
            do_log(chartType);

            switch (chartType) {
                case '2D':
                    this.Chart_2D(pie_data, data, do_log, max_x, max_y);
                    break;
                case '3D':
                    this.Chart_3D(pie_data, data, do_log, max_x, max_y);
                    break;
            }

        },

        Chart_3D: function(pie_data, data, do_log, max_x, max_y) {
            do_log("Process 3D chart");

        	function pieTop(d, rx, ry, ir ){
		        if(d.endAngle - d.startAngle === 0 ) return "M 0 0";
                var sx = rx*Math.cos(d.startAngle),
			        sy = ry*Math.sin(d.startAngle),
			        ex = rx*Math.cos(d.endAngle),
			        ey = ry*Math.sin(d.endAngle);

		        var ret =[];
		        ret.push("M",sx,sy,"A",rx,ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0),"1",ex,ey,"L",ir*ex,ir*ey);
        		ret.push("A",ir*rx,ir*ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0), "0",ir*sx,ir*sy,"z");
		        return ret.join(" ");
	        }

	        function pieOuter(d, rx, ry, h ){
		        var startAngle = (d.startAngle > Math.PI ? Math.PI : d.startAngle);
		        var endAngle = (d.endAngle > Math.PI ? Math.PI : d.endAngle);

		        var sx = rx*Math.cos(startAngle),
			        sy = ry*Math.sin(startAngle),
			        ex = rx*Math.cos(endAngle),
			        ey = ry*Math.sin(endAngle);

			        var ret =[];
			        ret.push("M",sx,h+sy,"A",rx,ry,"0 0 1",ex,h+ey,"L",ex,ey,"A",rx,ry,"0 0 0",sx,sy,"z");
			        return ret.join(" ");
	        }

	        function pieInner(d, rx, ry, h, ir ){
		        var startAngle = (d.startAngle < Math.PI ? Math.PI : d.startAngle);
		        var endAngle = (d.endAngle < Math.PI ? Math.PI : d.endAngle);

		        var sx = ir*rx*Math.cos(startAngle),
			        sy = ir*ry*Math.sin(startAngle),
			        ex = ir*rx*Math.cos(endAngle),
			        ey = ir*ry*Math.sin(endAngle);

                var ret =[];
			    ret.push("M",sx, sy,"A",ir*rx,ir*ry,"0 0 1",ex,ey, "L",ex,h+ey,"A",ir*rx, ir*ry,"0 0 0",sx,h+sy,"z");
			    return ret.join(" ");
	        }

            var xcenter = parseInt(this._getEscapedProperty("xcenter")) || 80;
            var ycenter = parseInt(this._getEscapedProperty("ycenter")) || 80;
            var xradius = parseInt(this._getEscapedProperty("xradius")) || 60;
            var yradius = parseInt(this._getEscapedProperty("yradius")) || 30;
            var height = parseInt(this._getEscapedProperty("height")) || 20;
            var iradius = parseFloat(this._getEscapedProperty("iradius")) || 0.4;
//            var chartSize = parseInt(this._getEscapedProperty("chartSize")) || 100,
//                padding = parseInt(this._getEscapedProperty("chartPadding")) || 5,
            var svg_width = parseInt(this._getEscapedProperty("svg_width")) || 1024,
                svg_height = parseInt(this._getEscapedProperty("svg_height")) || 1000,
                drilldown = this._check_boolean("drilldown"),
                legend_attr = {
                    "show": this._check_boolean("show_legend"),
                    "size": parseInt(this._getEscapedProperty("legend_height")) || 12,
                    "position": this._getEscapedProperty("legend_position") || "left",
                    "left_fudge": parseInt(this._getEscapedProperty("legend_left_fudge")) || -60,
                    "top_fudge": parseInt(this._getEscapedProperty("legend_top_fudge")) || 10,
                    "height": 350,
                    "width": 400
                };

            var myEl = this.el;
            do_log(myEl);
            do_log("creating element");
            do_log("Draw 3D Donut");

            this.$el.height(svg_height).width(svg_width);

            var translate = {"left": 80, "top": 100, "legend": {"left": 20, "top_fudge": legend_attr.top_fudge}};
            var _data = d3.layout.pie().value(function(d) {return d[1];}) (pie_data);

            do_log(_data);

            if (legend_attr.show) {
                if (legend_attr.position == "right") {
                    translate.left += 100;
//                    translate.legend.left = (chartSize + ((data.fields.length * padding) + max_x) + legend_attr.left_fudge);
                    translate.legend.left = (100 + max_x + legend_attr.left_fudge);
                }
                if (legend_attr.position == "left") {
                    translate.left += legend_attr.width + legend_attr.left_fudge;
                    translate.legend.left -= legend_attr.width + legend_attr.left_fudge;
                }

            }
            do_log(translate);

            var svg = d3.select(myEl).append('svg')
                .attr("width", svg_width)
                .attr("height", svg_height)
                .append("g")
                .attr("transform","translate(" + translate.left + "," + translate.top + ")");

            var slices = svg.append("g").attr("class", "slices");

            var color = d3.scale.category20();

            slices.selectAll(".innerSlice").data(_data).enter().append("path").attr("class", "innerSlice")
                .style("fill", function(d) { return d3.hsl(color(d.data[0])).darker(0.7); })
                .attr("d",function(d){ return pieInner(d, xcenter+0.5,ycenter+0.5, height, iradius);})
                .each(function(d){this._current=d;});

            slices.selectAll(".topSlice").data(_data).enter().append("path").attr("class", "topSlice")
                .style("fill", function(d) { return color(d.data[0]); })
                .style("stroke", function(d) { return color(d.data[0]); })
                .attr("d",function(d){ return pieTop(d, xcenter, ycenter, iradius);})
                .each(function(d){this._current=d;});

            slices.selectAll(".outerSlice").data(_data).enter().append("path").attr("class", "outerSlice")
                .style("fill", function(d) { return d3.hsl(color(d.data[0])).darker(0.7); })
                .attr("d",function(d){ return pieOuter(d, xcenter-0.5,ycenter-0.5, height);})
                .each(function(d){this._current=d;});

            var tooltip = d3.select(myEl)
                .append("div")
                .attr("class", "tooltip")
                .style("opacity", "0")
                .style("display","none");

            tooltip.append("div")
                .attr("class", "name");

            tooltip.append('div')
                .attr("class", "count");

            tooltip.append('div')
                .attr("class", "percent");

            if (legend_attr.show) {
                var legend = svg.append("svg")
                    .attr("class", "legend_container")
//                    .attr("height", ((chartSize * data.fields.length) + (data.fields.length * padding) + 50))
                    .attr("height", legend_attr.height)
                    .attr("width", legend_attr.width)
                    .attr("overflow", "auto")
                    .selectAll("g.legend")
                    .data(color.domain().slice().reverse())
                    .enter().append("svg:g")
                    .attr("class", "legend")
                    .attr("transform", function(d, i) {
                        return "translate(" + translate.legend.left + "," + (i * 20 + translate.legend.top_fudge) + ")";
                    });

                legend.append('rect')
                    .attr("width", 18)
                    .attr("height", 18)
                    .style("fill", color);

                legend.append('text')
                    .attr("x", 24)
                    .attr("y", 9)
                    .attr("font-size", legend_attr.size + "px")
                    .attr("dy", ".35em")
                    .text(function(d) {return d;});
            }

            var topSlice = svg.selectAll('.topSlice');

            if (drilldown) {
                topSlice.on('click', this._drilldown.bind(this));
            }
            topSlice.on('mouseover', function(d) {
                var total_events = d3.sum(_data, (function(d) {
                    return d.data[1];
                }));

                var event_count = 0;

                if (d.data[0] == "OTHER") {
                    for (var i = 0; i < data.rows.length; i++) {
                        if (data.rows[i][0] == "OTHER") {
                            event_count += parseInt(data.rows[i][1]);
                        }
                    }
                }
                else {
                    event_count = d.data[1];
                }

                var percent = Math.round(1000 * event_count / total_events /10);

                tooltip.style("opacity", "1")
                tooltip.style("display", "block")
                tooltip.select('.name').html("name: " + d.data[0]);
                tooltip.select('.count').html("count: " + event_count);
                tooltip.select('.percent').html("percent: " + percent + '%');
                tooltip.style('display', 'block');
            });

            topSlice.on('mouseout', function(d) {
                tooltip.style("opacity", "0")
                tooltip.style('display', 'none');
            });

        },

        Chart_2D: function(pie_data, data, do_log, max_x, max_y) {

            do_log("Process 2D chart");
            do_log(data);
            
            var myEl = this.el;
            do_log(myEl);
            do_log("creating element");
            do_log("Draw Donut");

            var donutWidth = parseInt(this._getEscapedProperty("donutWidth")) || 50;

//            var chartSize = parseInt(this._getEscapedProperty("chartSize")) || 100,
//                padding = parseInt(this._getEscapedProperty("chartPadding")) || 5,
            var svg_width = parseInt(this._getEscapedProperty("svg_width")) || 1024,
                svg_height = parseInt(this._getEscapedProperty("svg_height")) || 1000,
                radius = parseInt(this._getEscapedProperty("radius")) || 80,
                drilldown = this._check_boolean("drilldown"),
                legend_attr = {
                    "show": this._check_boolean("show_legend"),
                    "size": parseInt(this._getEscapedProperty("legend_height")) || 12,
                    "position": this._getEscapedProperty("legend_position") || "right",
                    "left_fudge": parseInt(this._getEscapedProperty("legend_left_fudge")) || -60,
                    "top_fudge": parseInt(this._getEscapedProperty("legend_top_fudge")) || 10,
                    "height": 350,
                    "width": 400
                };

            var color = d3.scale.category20();

            this.$el.height(svg_height).width(svg_width);

            var translate = {"left": 80, "top": 100, "legend": {"left": 20, "top_fudge": legend_attr.top_fudge}};
            var _data = d3.layout.pie().value(function(d) {return d[1];}) (pie_data);

            do_log(_data);

            if (legend_attr.show) {
                if (legend_attr.position == "right") {
                    translate.left += 100;
                    translate.legend.left = (100 + radius + max_x + legend_attr.left_fudge);
//                    translate.legend.left = (radius + ((data.fields.length * padding) + max_x) + legend_attr.left_fudge);
                }
                if (legend_attr.position == "left") {
                    translate.left += legend_attr.width + legend_attr.left_fudge;
                    translate.legend.left -= legend_attr.width + legend_attr.left_fudge;
                }

            }
            do_log(translate);

            var arc = d3.svg.arc()
                .innerRadius(radius - donutWidth)
                .outerRadius(radius);

            var arcOver = d3.svg.arc()
                .innerRadius((radius - donutWidth) + 5)
                .outerRadius(radius + 5);

            var tooltip = d3.select(myEl)
                .append("div")
                .attr("class", "tooltip")
                .style("opacity", "0")
                .style("display","none");

            tooltip.append("div")
                .attr("class", "name");

            tooltip.append('div')
                .attr("class", "count");

            tooltip.append('div')
                .attr("class", "percent");

            var svg = d3.select(myEl).append('svg')
                .attr("width", svg_width)
                .attr("height", svg_height)
                .append("g")
                .attr("transform","translate(" + translate.left + "," + translate.top +  ")");

            do_log(svg);

            var path = svg.selectAll('path')
                .data(_data)
                .enter()
                .append('path')
                .attr('d',arc)
                .attr('fill', function(d) {
                    return color(d.data[0]);
                });

            if (drilldown) {
                path.on('click', this._drilldown.bind(this));
            }
            path.on('mouseover', function(d) {
                var total_events = d3.sum(_data, (function(d) {
                    return d.data[1];
                }));

                var event_count = 0;

                if (d.data[0] == "OTHER") {
                    for (var i = 0; i < data.rows.length; i++) {
                        if (data.rows[i][0] == "OTHER") {
                            event_count += parseInt(data.rows[i][1]);
                        }
                    }
                }
                else {
                    event_count = d.data[1];
                }

                var percent = Math.round(1000 * event_count / total_events /10);
                d3.select(this)
                    .transition()
                    .duration(200)
                    .attr("d",arcOver)
                tooltip.style("opacity", "1")
                tooltip.style("display", "block")
                tooltip.select('.name').html("name: " + d.data[0]);
                tooltip.select('.count').html("count: " + event_count);
                tooltip.select('.percent').html("percent: " + percent + '%');
                tooltip.style('display', 'block');
            });

            path.on('mouseout', function(d) {
                d3.select(this)
                    .transition()
                    .attr("d",arc)
                tooltip.style("opacity", "0")
                tooltip.style('display', 'none');
            });

            if (legend_attr.show) {
                var legend = svg.append("svg")
                    .attr("class", "legend_container")
//                    .attr("height", ((chartSize * data.fields.length) + (data.fields.length * padding) + 50))
                    .attr("height", legend_attr.height)
                    .attr("width", legend_attr.width)
                    .attr("overflow", "auto")
                    .selectAll("g.legend")
                    .data(color.domain().slice().reverse())
                    .enter().append("svg:g")
                    .attr("class", "legend")
                    .attr("transform", function(d, i) {
                        return "translate(" + translate.legend.left + "," + (i * 20 + translate.legend.top_fudge) + ")";
                    });

                legend.append('rect')
                    .attr("width", 18)
                    .attr("height", 18)
                    .style("fill", color);

                legend.append('text')
                    .attr("x", 24)
                    .attr("y", 9)
                    .attr("font-size", legend_attr.size + "px")
                    .attr("dy", ".35em")
                    .text(function(d) {return d;});
            }

        },

        // Search data params
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.ROW_MAJOR_OUTPUT_MODE,
                count: 10000
            });
        },

        // Override to respond to re-sizing events
        reflow: function() {
            this.invalidateUpdateView();
        },

        _getEscapedProperty: function(name) {
            var propertyValue = this._config[this.getPropertyNamespaceInfo().propertyNamespace + name];
            return vizUtils.escapeHtml(propertyValue);
        },

        _drilldown: function(d, i) {
            var fields = this.getCurrentData().fields;
            var drilldownDescription = {
                action: SplunkVisualizationBase.FIELD_VALUE_DRILLDOWN,
                data: {}
            };

            drilldownDescription.data[fields[0].name] = d.data[0];
            this.drilldown(drilldownDescription, d3.event);
        }

    });
});