define([
            'jquery',
            'underscore',
            'api/SplunkVisualizationBase',
            'api/SplunkVisualizationUtils',
            'd3'
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            SplunkVisualizationUtils,
            d3
        ) {
    return SplunkVisualizationBase.extend({

        initialize: function() {
            // Save this.$el for convenience
            this.$el = $(this.el);
             
            // Add a css selector class
            this.$el.addClass('waterfall-custom-viz');
        },
 
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.RAW_OUTPUT_MODE,
                count: 10000
            });
        },formatData: function(data, config) {	
			var nameField;
			var valueField;

			//Exit if no Data
			if (!data) {
				throw new SplunkVisualizationBase.VisualizationError(
					'No Results Found. Validate Search.'
				);
				return;
			}else if (data.fields.length < 1) {
                return false;
            }else if (data.fields.length!==2 && data.fields.length!==5){
				throw new SplunkVisualizationBase.VisualizationError(
					'Check the Statistics tab. Transforming command should strictly return two columns - name and value.'
				);
			}else{
				var nameField=SplunkVisualizationUtils.escapeHtml(data.fields[0].name); //First Column contains Field Name (name or key)
				var valueField=SplunkVisualizationUtils.escapeHtml(data.fields[1].name); //Seconf Column contains Field Value (value)
			}
			//Read Data to get Total
			var cumulative=0;
			
			for (var rowCounter = 0; rowCounter < data.results.length; rowCounter++) {
				
				//Format Data for Visualization
				data.results[rowCounter].name=SplunkVisualizationUtils.escapeHtml(data.results[rowCounter][nameField]); // Set Name/Key as name in data results array
				data.results[rowCounter].value=parseFloat(SplunkVisualizationUtils.escapeHtml(data.results[rowCounter][valueField])); // Set Value as value in data results array
			
				data.results[rowCounter].start = cumulative;
				cumulative += parseFloat(SplunkVisualizationUtils.escapeHtml(data.results[rowCounter].value));
				// Check for invalid data
				if(_.isNaN(cumulative)){
					//debugMsg ='Second column should be numeric. Could not assign numeric value to cumulative field.';
					throw new SplunkVisualizationBase.VisualizationError(
						'Second data column should be numeric.'
					);
				}
				data.results[rowCounter].end = cumulative;
				//Assign positive and negative class only if total class is already not present.
				//total class will either be last row of array or not present 
				if(data.results[rowCounter].class!='total'){
					data.results[rowCounter].class = parseFloat(SplunkVisualizationUtils.escapeHtml(data.results[rowCounter].value)) >= 0 ? 'positive' : 'negative'
				}
			}
			//Last iterated data array is always Total
			if (!(data.results[rowCounter-1].name==='Total')){
				throw new SplunkVisualizationBase.VisualizationError(
					'Pipe the following commmand to your transforming command. Ensure Total is the final field name "| addcoltotals "'
				);
			} else {	
				data.results[rowCounter-1].start=0;
				data.results[rowCounter-1].end=parseFloat(SplunkVisualizationUtils.escapeHtml(data.results[rowCounter-1].value));
				data.results[rowCounter-1].class='total';
				return data;
			}
		},
        updateView: function(data, config) {
			//Exit if no Data
			if (!data) {
				return;
			}else if (data.fields.length < 1) {
                return;
			}else if (data.fields.length!==2 && data.fields.length!==5){
				return false;
			}
            // Clear the div
            this.$el.empty();

			// Load Chart Configurations or set defaults
			//Set the positiveColor from config or default positiveColor to #31a35f (Green)
			var positiveColor= SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'positiveColor'] || '#31a35f');
			//Set the negativeColor from config or default negativeColor to #d6563c (Red)
			var negativeColor=SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'negativeColor'] || '#d6563c');
			//Set the totalColor from config or default totalColor to #1e93c6 (Blue)
			var totalColor= SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'totalColor'] || '#1e93c6');
			//Set the connectorColor from config or default connectorColor to #F7BC38 (Yellow)
			var connectorColor=SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'connectorColor'] || '#F7BC38');
			//Set the barTextColor from config or default barTextColor to #555555 (Grey)
			var barTextColor=SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'barTextColor'] || '#555555');
			//Set the Unit from config or default Unit to dollar ($)
			var valueUnit= SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'valueUnit'] || '$');
			//Set the Unit Position from config or default Unit to be displayed before or after value or no Unit
			var unitPosition=config[this.getPropertyNamespaceInfo().propertyNamespace + 'unitPosition'] || 'none';
			//Set the width from config or default width to 800
			var width=SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'width'] || '800');
			//Set the height from config or default height to 300
			var height=SplunkVisualizationUtils.escapeHtml(config[this.getPropertyNamespaceInfo().propertyNamespace + 'height'] || '300');
			//Set the showBarText from config or default showBarText to true
			var showBarText=config[this.getPropertyNamespaceInfo().propertyNamespace + 'showBarText'] || 'show'	;

			//If height is not numeric or less than zero, Chart can not be plotted. So defaulting height.
			if(isNaN(height)||(height<0)){		
				height=800;
			}			
			//If width is not numeric or less than zero, Chart can not be plotted. So defaulting width.
			if(isNaN(width)||(width<0)){
				width=300;
			}
			
			//Set Visualization Area - Margin, Height, Width and Padding 
			var margin = {top: 15, right: 15, bottom: 30, left: 40},
				width = width - margin.left - margin.right,
				height = height - margin.top - margin.bottom,
				padding = 0.3;

			var x = d3.scale.ordinal()
				.rangeRoundBands([0, width],padding);
			//Set x-axis svg labels
			x.domain(data.results.map(function(d) { return d.name; }));
			var xAxis = d3.svg.axis()
				.scale(x)
				.orient("bottom");

			var y = d3.scale.linear()
				.range([height, 0]);
			//Set y-axis svg labels	
			y.domain([0, d3.max(data.results, function(d) { return d.end; })]);	
			var yAxis = d3.svg.axis()
				.scale(y)
				.orient("left")
				.tickFormat(function(d) { return yAxisLabelFormatter(d,valueUnit,unitPosition); });
			
			//Initialize svg section. Add to current div element
			var svg = d3.select(this.el).append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
				.style('background', 'white')
				.append("g")
				.attr("transform", "translate(" + 2*(margin.left) + "," + margin.top + ")");

			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);

			svg.append("g")
				.attr("class", "y axis")
				.call(yAxis);

			var bar = svg.selectAll(".bar")
				.data(data.results)
				.enter().append("g")
				.attr("class", function(d) { return "bar " + d.class })
				.attr("transform", function(d) { return "translate(" + x(d.name) + ",0)"; });

			bar.append("rect")
				.attr("y", function(d) { return y( Math.max(d.start, d.end) ); })
				.attr("x", x.rangeBand()/4)
				.attr("height", function(d) { return Math.abs( y(d.start) - y(d.end) ); })
				.attr("width", x.rangeBand()/2)
				.style("fill", function(d) { return ((d.class=='negative') ? negativeColor : ((d.class=='positive') ? positiveColor :totalColor)) });//Fill color set to Negative Positive or Total based on bar class
			
			if(showBarText==="show"){
				bar.append("text")
					.attr("x", x.rangeBand() / 2)
					.attr("y", function(d) { return y(d.end) + 5; })
					.attr("dy", function(d) { return ((d.class=='negative') ? '-' : '') + '.75em' })
					.text(function(d) { return yAxisLabelFormatter(d.end - d.start,valueUnit,unitPosition);})
					.style('fill',barTextColor);
			}

			bar.filter(function(d) { return d.class != "total" }).append("line")
				.attr("class", "connector")
				.attr("x1", x.rangeBand() + 5 )
				.attr("y1", function(d) { return y(d.end) } )
				.attr("x2", x.rangeBand() / ( 1 - padding) - 5 )
				.attr("y2", function(d) { return y(d.end) } )
				.style("stroke",connectorColor);

			//Utility Formatter for Y-Axis (adds $ sign)
			function yAxisLabelFormatter(n,valueUnit,unitPosition) {
				n = Math.round(n);
				var result = n;
				//If Greater than 1000 Convert to K
				if (Math.abs(n) > 1000) {
					result = Math.round(n/1000) + 'K';
				}
				switch (unitPosition) {
					case 'before':
						return valueUnit+result;
						break;
					case 'after':
						return result+valueUnit;
						break;
					default:
						return result;
				}
			}	
		}
    });
});
