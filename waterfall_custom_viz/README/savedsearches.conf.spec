
# Declare properties here
display.visualizations.custom.waterfall_custom_viz.waterfall.positiveColor = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.negativeColor = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.totalColor = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.connectorColor = <string>
### x-axis label rotation to be implemented later
### display.visualizations.custom.waterfall_custom_viz.waterfall.xAxisLabelRotation = <int> 
display.visualizations.custom.waterfall_custom_viz.waterfall.valueUnit = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.unitPosition = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.showBarText = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.barTextColor = <string>
display.visualizations.custom.waterfall_custom_viz.waterfall.height = <int>
display.visualizations.custom.waterfall_custom_viz.waterfall.width = <int>