# Waterfall Custom Visualization App

 Waterfall chart is used to show the cumulative effect of sequentially introduced positive or negative values. https://en.wikipedia.org/wiki/Waterfall_chart
 It is D3 Visualization based on Chuck Lams's block http://bl.ocks.org/chucklam/f3c7b3e3709a0afd5d57
- The relevant directory structure for a visuzliation app
- Waterfall Custom visualization package directory is located under $SPLUNK_HOME\etc\apps\waterfall_custom_viz\appserver\static\visualizations\waterfall
- Relevant .conf files are placed under required directory structure

# savedsearches.conf 
$SPLUNK_HOME\etc\apps\waterfall_custom_viz\default\savedsearches.conf has configurable properties defined for SimpleXML and SplunkJS

## Future Enhancements

The visualization current does not have following features:
	- x-axis label rotation
	- Tooltip text on mouseover
	- Drilldown on mouse click
	- Dynamic Visualization size