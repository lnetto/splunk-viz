/*
 * Open source under BSD 2-clause
 * Copyright (c) 2015, Curtis Bratton
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
define([
            'jquery',
            'underscore',
            'vizapi/SplunkVisualizationBase',
            'vizapi/SplunkVisualizationUtils',
            'd3'
            // Add required assets to this list
        ],
        function(
            $,
            _,
            SplunkVisualizationBase,
            SplunkVisualizationUtils,
            d3
        ) {

    // Extend from SplunkVisualizationBase
    return SplunkVisualizationBase.extend({

        initialize: function() {
            // SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
            this.$el = $(this.el);

            // this.$el.append('<h3>This is a custom visualization stand in.</h3>');
            // this.$el.append('<p>Edit your custom visualization app to render something here.</p>');

            // Add a css selector class
            this.$el.addClass('splunk-water-gauge');

            // Initialization logic goes here
        },

        // Optionally implement to format data returned from search.
        // The returned object will be passed to updateView as 'data'
        // formatData: function(data) {
        //
        //     // Format data
        //
        //     return data;
        // },

        // Implement updateView to render a visualization.
        //  'data' will be the data object returned from formatData or from the search
        //  'config' will be the configuration property object
        updateView: function(data, config) {

            // Draw something here
            if(data.rows.length < 1){
                return;
            }
            // Take the first data point
            datum = data.rows[0][0];
            // Clear the div
            this.$el.empty();

            // Set height and width
            var height = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.height'] || 220;
            var width = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.width'] || 220;

            // SVG setup
            var svg  = d3.select(this.el).append('svg')
                .attr('id', 'watergauge-'+datum)
                .attr('width', width)
                .attr('height', height)
                .style('background', 'white');

            // The gauge minimum value.
            var minValue = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.minValue'] || 0;
            // The gauge maximum value.
            var maxValue = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.maxValue'] || 100;
            // The outer circle thickness as a percentage of it's radius.
            var circleThickness = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.circleThickness'] || 0.05;
            // The size of the gap between the outer circle and wave circle as a percentage of the outer circles radius.
            var circleFillGap = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.circleFillGap'] || 0.05;
            // The color of the outer circle.
            var circleColor = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.circleColor'] || "#178BCA";
            // The wave height as a percentage of the radius of the wave circle.
            var waveHeight = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveHeight'] || 0.1;
            // The number of full waves per width of the wave circle.
            var waveCount = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveCount'] || 1;
            // The amount of time in milliseconds for the wave to rise from 0 to it's final height.
            var waveRiseTime = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveRiseTime'] || 1000;
            // The amount of time in milliseconds for a full wave to enter the wave circle.
            var waveAnimateTime = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveAnimateTime'] || 50000;
            // Control if the wave should rise from 0 to it's full height, or start at it's full height.
            var waveRise = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveRise'] || "true";
            // Controls wave size scaling at low and high fill percentages. When true, wave height reaches it's maximum at 50% fill, and minimum at 0% and 100% fill. This helps to prevent the wave from making the wave circle from appear totally full or empty when near it's minimum or maximum fill.
            var waveHeightScaling = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveHeightScaling'] || "true";
            // Controls if the wave scrolls or is static.
            var waveAnimate = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveAnimate'] || "true";
            // The color of the fill wave.
            var waveColor = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveColor'] || "#178BCA";
            // The amount to initially offset the wave. 0 = no offset. 1 = offset of one full wave.
            var waveOffset = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveOffset'] || 0;
            // The height at which to display the percentage text withing the wave circle. 0 = bottom, 1 = top.
            var textVertPosition = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.textVertPosition'] || .5;
            // The relative height of the text to display in the wave circle. 1 = 50%
            var textSize = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.textSize'] || 1;
            // If true, the displayed value counts up from 0 to it's final value upon loading. If false, the final value is displayed.
            var valueCountUp = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.valueCountUp'] || "true";
            // If true, a % symbol is displayed after the value.
            var displayPercent = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.displayPercent'] || "true";
            // The color of the value text when the wave does not overlap it.
            var textColor = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.textColor'] || "#045681";
            // The color of the value text when the wave overlaps it.
            var waveTextColor = config['display.visualizations.custom.cuviz_water_gauge.water_gauge.waveTextColor'] || "#A4DBf8";

            var gauge = svg;
            var radius = Math.min(parseInt(gauge.style("width")), parseInt(gauge.style("height")))/2;
            var locationX = parseInt(gauge.style("width"))/2 - radius;
            var locationY = parseInt(gauge.style("height"))/2 - radius;
            var fillPercent = Math.max(minValue, Math.min(maxValue, datum))/maxValue;
            var waveHeightScale;
            if(waveHeightScaling === "true"){
                waveHeightScale = d3.scale.linear()
                    .range([0,waveHeight,0])
                    .domain([0,50,100]);
            } else {
                waveHeightScale = d3.scale.linear()
                    .range([waveHeight,waveHeight])
                    .domain([0,100]);
            }

            var textPixels = (textSize*radius/2);
            var textFinalValue = parseFloat(datum).toFixed(2);
            var textStartValue = (valueCountUp === "true")?minValue:textFinalValue;
            var percentText = (displayPercent === "true")?"%":"";
            var circleThickness = circleThickness * radius;
            var circleFillGap = circleFillGap * radius;
            var fillCircleMargin = circleThickness + circleFillGap;
            var fillCircleRadius = radius - fillCircleMargin;
            var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);
            var waveLength = fillCircleRadius*2/waveCount;
            var waveClipCount = 1+waveCount;
            var waveClipWidth = waveLength*waveClipCount;

            // Rounding functions so that the correct number of decimal places is always displayed as the value counts up.
            var textRounder = function(value){ return Math.round(value); };
            if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
                textRounder = function(value){ return parseFloat(value).toFixed(1); };
            }
            if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
                textRounder = function(value){ return parseFloat(value).toFixed(2); };
            }

            // Data for building the clip wave area.
            var dataWave = [];
            for(var i = 0; i <= 40*waveClipCount; i++){
                dataWave.push({x: i/(40*waveClipCount), y: (i/(40))});
            }

            // Scales for drawing the outer circle.
            var gaugeCircleX = d3.scale.linear().range([0,2*Math.PI]).domain([0,1]);
            var gaugeCircleY = d3.scale.linear().range([0,radius]).domain([0,radius]);

            // Scales for controlling the size of the clipping path.
            var waveScaleX = d3.scale.linear().range([0,waveClipWidth]).domain([0,1]);
            var waveScaleY = d3.scale.linear().range([0,waveHeight]).domain([0,1]);

            // Scales for controlling the position of the clipping path.
            var waveRiseScale = d3.scale.linear()
                // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
                // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
                // circle at 100%.
                .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
                .domain([0,1]);
            var waveAnimateScale = d3.scale.linear()
                .range([0, waveClipWidth-fillCircleRadius*2]) // Push the clip area one full wave then snap back.
                .domain([0,1]);

            // Scale for controlling the position of the text within the gauge.
            var textRiseScaleY = d3.scale.linear()
                .range([fillCircleMargin+fillCircleRadius*2,(fillCircleMargin+textPixels*0.7)])
                .domain([0,1]);

            // Center the gauge within the parent SVG.
            var gaugeGroup = gauge.append("g")
                .attr('transform','translate('+locationX+','+locationY+')');

            // Draw the outer circle.
            var gaugeCircleArc = d3.svg.arc()
                .startAngle(gaugeCircleX(0))
                .endAngle(gaugeCircleX(1))
                .outerRadius(gaugeCircleY(radius))
                .innerRadius(gaugeCircleY(radius-circleThickness));
            gaugeGroup.append("path")
                .attr("d", gaugeCircleArc)
                .style("fill", circleColor)
                .attr('transform','translate('+radius+','+radius+')');

            // Text where the wave does not overlap.
            var text1 = gaugeGroup.append("text")
                .text(textRounder(textStartValue) + percentText)
                .attr("class", "liquidFillGaugeText")
                .attr("text-anchor", "middle")
                .attr("font-size", textPixels + "px")
                .style("fill", textColor)
                .attr('transform','translate('+radius+','+textRiseScaleY(textVertPosition)+')');

            // The clipping wave area.
            var clipArea = d3.svg.area()
                .x(function(d) { return waveScaleX(d.x); } )
                .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*waveOffset*-1 + Math.PI*2*(1-waveCount) + d.y*2*Math.PI));} )
                .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
            var waveGroup = gaugeGroup.append("defs")
                .append("clipPath")
                .attr("id", "clipWave-"+datum);
            var wave = waveGroup.append("path")
                .datum(dataWave)
                .attr("d", clipArea)
                .attr("T", 0);

            // The inner circle with the clipping wave attached.
            var fillCircleGroup = gaugeGroup.append("g")
                .attr("clip-path", "url(#clipWave-"+datum+")");
            fillCircleGroup.append("circle")
                .attr("cx", radius)
                .attr("cy", radius)
                .attr("r", fillCircleRadius)
                .style("fill", waveColor);

            // Text where the wave does overlap.
            var text2 = fillCircleGroup.append("text")
                .text(textRounder(textStartValue) + percentText)
                .attr("class", "liquidFillGaugeText")
                .attr("text-anchor", "middle")
                .attr("font-size", textPixels + "px")
                .style("fill", waveTextColor)
                .attr('transform','translate('+radius+','+textRiseScaleY(textVertPosition)+')');

            // Make the value count up.
            if(valueCountUp === "true"){
                var textTween = function(){
                    var i = d3.interpolate(this.textContent, textFinalValue);
                    return function(t) { this.textContent = textRounder(i(t)) + percentText; }
                };
                text1.transition()
                    .duration(waveRiseTime)
                    .tween("text", textTween);
                text2.transition()
                    .duration(waveRiseTime)
                    .tween("text", textTween);
            }

            // Make the wave rise. wave and waveGroup are separate so that horizontal and vertical movement can be controlled independently.
            var waveGroupXPosition = fillCircleMargin+fillCircleRadius*2-waveClipWidth;
            if(waveRise === "true"){
                waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(0)+')')
                    .transition()
                    .duration(waveRiseTime)
                    .attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')')
                    .each("start", function(){ wave.attr('transform','translate(1,0)'); }); // This transform is necessary to get the clip wave positioned correctly when waveRise=true and waveAnimate=false. The wave will not position correctly without this, but it's not clear why this is actually necessary.
            } else {
                waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')');
            }

            if(waveAnimate === "true") animateWave();

            function animateWave() {
                wave.attr('transform','translate('+waveAnimateScale(wave.attr('T'))+',0)');
                wave.transition()
                    .duration(waveAnimateTime * (1-wave.attr('T')))
                    .ease('linear')
                    .attr('transform','translate('+waveAnimateScale(1)+',0)')
                    .attr('T', 1)
                    .each('end', function(){
                        wave.attr('T', 0);
                        animateWave(waveAnimateTime);
                    });
            }

            function GaugeUpdater(){
                this.update = function(value){
                    var newFinalValue = parseFloat(value).toFixed(2);
                    var textRounderUpdater = function(value){ return Math.round(value); };
                    if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                        textRounderUpdater = function(value){ return parseFloat(value).toFixed(1); };
                    }
                    if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                        textRounderUpdater = function(value){ return parseFloat(value).toFixed(2); };
                    }

                    var textTween = function(){
                        var i = d3.interpolate(this.textContent, parseFloat(value).toFixed(2));
                        return function(t) { this.textContent = textRounderUpdater(i(t)) + percentText; }
                    };

                    text1.transition()
                        .duration(waveRiseTime)
                        .tween("text", textTween);
                    text2.transition()
                        .duration(waveRiseTime)
                        .tween("text", textTween);

                    var fillPercent = Math.max(minValue, Math.min(maxValue, value))/maxValue;
                    var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);
                    var waveRiseScale = d3.scale.linear()
                        // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
                        // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
                        // circle at 100%.
                        .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
                        .domain([0,1]);
                    var newHeight = waveRiseScale(fillPercent);
                    var waveScaleX = d3.scale.linear().range([0,waveClipWidth]).domain([0,1]);
                    var waveScaleY = d3.scale.linear().range([0,waveHeight]).domain([0,1]);
                    var newClipArea;
                    if(waveHeightScaling === "true"){
                        newClipArea = d3.svg.area()
                            .x(function(d) { return waveScaleX(d.x); } )
                            .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*waveOffset*-1 + Math.PI*2*(1-waveCount) + d.y*2*Math.PI));} )
                            .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
                    } else {
                        newClipArea = clipArea;
                    }

                    var newWavePosition = waveAnimate?waveAnimateScale(1):0;
                    wave.transition()
                        .duration(0)
                        .transition()
                        .duration(waveAnimate?(waveAnimateTime * (1-wave.attr('T'))):(waveRiseTime))
                        .ease('linear')
                        .attr('d', newClipArea)
                        .attr('transform','translate('+newWavePosition+',0)')
                        .attr('T','1')
                        .each("end", function(){
                            if(waveAnimate === "true"){
                                wave.attr('transform','translate('+waveAnimateScale(0)+',0)');
                                animateWave(waveAnimateTime);
                            }
                        });
                    waveGroup.transition()
                        .duration(waveRiseTime)
                        .attr('transform','translate('+waveGroupXPosition+','+newHeight+')')
                }
            }

            return new GaugeUpdater();
        },

        // Search data params
        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.ROW_MAJOR_OUTPUT_MODE,
                count: 10000
            });
        }

        // Override to respond to re-sizing events
        // reflow: function() {}


    });

});
