# Declare properties here
display.visualizations.custom.cuviz_water_gauge.water_gauge.height = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.width = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.minValue = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.maxValue = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.circleThickness = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.circleFillGap = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.circleColor = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveHeight = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveCount = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveRiseTime = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveAnimateTime = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveRise = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveHeightScaling = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveAnimate = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveColor = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveOffset = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.textVertPosition = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.textSize = <float>
display.visualizations.custom.cuviz_water_gauge.water_gauge.valueCountUp = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.displayPercent = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.textColor = <string>
display.visualizations.custom.cuviz_water_gauge.water_gauge.waveTextColor = <string>

display.visualizations.custom.cuviz_water_gauge.status_matrix.radius = <float>
