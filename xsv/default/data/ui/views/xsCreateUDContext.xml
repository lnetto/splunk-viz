<dashboard stylesheet="xsv.css">
  <label>xsCreateUDContext</label>
  <row>
    <html>
      <p>
        <br />
        <span class="head4">Command: </span>
        <span class="cmd">xsCreateUDContext</span>
      </p>
      <p>
        <span class="head4">Category: </span>
        <span class="bodytext">Conceptual Search</span>
      </p>
      <p>
        <span class="head4">Description: </span>
        <span class="bodytext">Creates a 'User Defined' Context with specified parameters.  The fields type, count, min, and max must be passed in as parameters.  If the type is 'domain', then the fields avg, median and size will be ignored.  If the type is 'average_centered', then the fields count, median, min and max will be ignored.  If the type is 'median_centered', then the fields avg, count, min and max will be ignored.  See <a href="xsCreateDDContext">xsCreateDDContext</a> for details about creating a Context using data from a search instead of 'User Defined' data. </span>
      </p>
      <p>
        <span class="head4">Syntax: </span>
        <span class="bodytext">xsCreateUDContext name=string
          terms=&quot;string(,string)*&quot;
          count=int
          [class=&quot;string(,string)*&quot;]
          [container=string]
          [endshape=(curve|linear)]
          [notes=string]
          [read=string]
          [save=(true|false)]
          [scope=(private|app|global)]
          [search=string]
          [shape=(pi|trapezoid|triangle|curveincrease|curvedecrease|linearincrease|lineardecrease)]
          (type=average_centered avg=double size=double | type=average_centered median=double
          size=double | type=domain min=double max=double)
          [uom=string]
          [write=string]
        </span>
      </p>
      <p>
        <br />
        <span class="head4">Example: </span>
        <span class="code">xsCreateUDContext name=ArrDelay terms=&quot;early,ontime,late&quot; type=average_centered avg=0 count=10000 size=30 max=100 min=0 uom=minutes</span>
      </p>
      <p>
        <span class="explanation">Creates an Average-centered Context, centered at 0, with 3 terms (early, ontime, late) using minutes as the unit of measure (UOM).</span>
      </p>
      <p>
        <span class="head4">Example: </span>
        <span class="code">| xsCreateUDContext name=AirTime type=domain terms=&quot;minimal,short,medium,long,extended&quot; min=60 max=1400 count=1000 avg=0 size=0 uom=minutes</span>
      </p>
      <p>
        <span class="explanation">Creates an Domain Context with the min/max and count specified, the unit of measure is minutes.</span>
      </p>
      <p>
        <span class="head4">Parameters: </span>
        <span class="bodytext"> avg, class, container, count, endshape, max, median, min, name, notes, read, scope, search, shape, size, terms, type, uom, writem</span>
      </p>
      <p>
        <span class="para">avg</span>
      </p>
      <p>
        <span class="explanation">The average value of the field.</span>
      </p>
      <p>
        <span class="para">class</span>
      </p>
      <p>
        <span class="explanation">A comma separated list of classes.</span>
      </p>
      <p>
        <span class="para">container</span>
      </p>
      <p>
        <span class="explanation">The container to store the context.</span>
      </p>
      <p>
        <span class="para">count</span>
      </p>
      <p>
        <span class="explanation">The number of fields.</span>
      </p>
      <p>
        <span class="para">endshape</span>
      </p>
      <p>
        <span class="explanation">The shape of the end concepts.  This defaults to curve.</span>
      </p> 
      <p>
        <span class="para">max</span>
      </p>
      <p>
        <span class="explanation">The maximum value of the field.</span>
      </p>
      <p>
        <span class="para">median</span>
      </p>
      <p>
        <span class="explanation">The median value of the field.</span>
      </p>
      <p>
        <span class="para">min</span>
      </p>
      <p>
        <span class="explanation">The minimum value of the field.</span>
      </p>
      <p>
        <span class="para">name (Context name)</span>
      </p>
      <p>
        <span class="explanation">A legal string which specifies the name by which the created Context will be known.</span>
      </p>
      <p>
        <span class="para">notes</span>
      </p>
      <p>
        <span class="explanation">A description of the context.  This defaults to 'none'.</span>
      </p> 
      <p>
        <span class="para">read</span>
      </p>
      <p>
        <span class="explanation">Read permissions (i.e., roles) .  This defaults to * (for all roles).</span>
      </p> 
      <p>
        <span class="para">save</span>
      </p>
      <p>
        <span class="explanation">Save the context after it is created.  The default value is "true". To turn off save, set save=false.</span>
      </p>
      <p>
        <span class="para">scope</span>
      </p>
      <p>
        <span class="explanation">Where to save the context (private, app, global).  This defaults to global.</span>
      </p> 
      <p>
        <span class="para">search</span>
      </p>
      <p>
        <span class="explanation">The search string used to generate data</span>
      </p>
      <p>
        <span class="para">shape</span>
      </p>
      <p>
        <span class="explanation">The shape of the middle concepts.  This defaults to pi.</span>
      </p> 
      <p>
        <span class="para">size</span>
      </p>
      <p>
        <span class="explanation">The size of a concept (before multiplying by width).</span>
      </p>
      <p>
        <span class="para">terms (array of term names)</span>
      </p>
      <p>
        <span class="explanation">A comma delimited set of legal strings which define the names of the terms within the Context.</span>
      </p>
      <p>
        <span class="para">type (context type)</span>
      </p>
      <p>
        <span class="explanation">average_centered' - centers the context on the average and uses std dev to scale term sizes. </span>
      </p>
      <p>
        <span class="explanation">domain - the default, centers the set on (max-min)/2 and scales the context to cover the whole domain. </span>
      </p>
      <p>
        <span class="para">uom</span>
      </p>
      <p>
        <span class="explanation">Unit of measure.</span>
      </p> 
      <p>
        <span class="para">write</span>
      </p>
      <p>
        <span class="explanation">Write permissions (i.e., roles) .  This defaults to * (for all roles).</span>
      </p> 
      <p>
        <span class="head4">Result: </span>
        <span class="bodytext">The context created by xsCreateUDContext is displayed in table or chart format.  See <a href="xsDisplayContext">xsDisplayContext</a> for details about display of the created Context. <br /> See <a href="xsCreateDDContext">xsCreateDDContext</a> for details about creating a Context using a 'streaming' command instead of a 'generating' command. </span>
      </p>
<br /><br />
      <a class="btn btn-xtreme" href="command_reference"> <span class="icon-play"></span> Return to Command Reference </a> 
      <p><br /><br />
        <span class="head4">Learn more: </span>
        <span class="bodytext">
          <a href="http://www.sciantaanalytics.com/learn" target="_blank">Scianta Analytics Learning Center</a>
        </span>
      </p><br /><br />
      <p>VERSION: 20150816.1<br />
      Custom application development for Splunk by <a href="http://www.concanon.com/splunkdev"
          target="_blank"> Concanon LLC.</a><br />
	Some portions of Extreme Search for Splunk are patented under US Patent 9,087,090.<br />
Copyright 2015 Scianta Analytics LLC. All Rights Reserved.</p>
    </html>
  </row>
</dashboard>
