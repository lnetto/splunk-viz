Scianta Analytics Extreme Search Visualization

Version:        1.2.2

Release Notes: 

1.2.2:
    - Fix access to xsUpdateADContext and xsUpdateCDContext

1.2.1:
    - Certified by Splunk for Cloud and Splunkbase

1.2.0:
    - Security Audit Fix to handle Log Injection
    - Security Audit Fixes to handle Improper Resource Shutdown or Release
    - Security Audit Fixes to handle Reflected Cross-Site Scripting (XSS)
    - Add escaping quotes (""") to CSV parser
    - Remove quotes for BY clause in stats command

1.1.0:
    - add filter for Class on Context Explorer
    - update browser address history based on choices in Context Explorer
    - update detail summaries on Context Explorer on selection.
    - prevent scope choices on views from being reset after search
    - update search string to return all contexts.
    - remove function param initializers not supported by Safari
    - add event limit choice on overlay data view
    - reload explorer if carousel doesn't load

1.0.10:
    - Security enhancements

1.0.9:
    - add xsvLookupContext command
    - On data overlay, display message on search when no data returned
    - On data overlay, provide event count progress message when searching
    - Include "field" in label for contexts
    - Update filter allowing numbers, dots, and sign char for number fields
    - Update filter allowing [a-zA-Z0-9-_ for name fields
    - Optimize loading of context data on Context Explorer
    - Optimize loading of app icons on Context Explorer
    - Prevent click ahead on Context Explorer
    - Wrap BY clauses with quotes
    - Handle multiple quotes on individual csv fields
    - Display red for outliers on AD context displays
    - Update concept labels on update in redefine context
    - Don't allow user to change concept type to custom on redefine context
    - Add CSRF validation check in searchUtil

1.0.4:
    - Only display Apps that have one or more context containers.
    - Eliminate duplicate entries error msg in dropdowns (use dedup)

1.0.3:
    - Enhancements for AppList/scope

1.0.2:
    - Aded Display Context dashboard

1.0.1:
    Initial Release with the following views:
    - Create Context
    - Update Context
    - Overlay Context
    - Compare Concepts
    - Relevance Overview
    - Command Reference

Known Isssues: 

    - searches stored in context are truncated at 256 chars
